# Page TSConfig:
tx_gridelements.setup {
	# ID of Element
	1900 {
		title = Row
		description = For use with responsive column width.
		config {
			colCount = 1
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.columnElements
							colPos = 111
						}
					}
				}
			}
		}
		icon = EXT:bootstrap_grids/Resources/Public/Icons/gridlayout_row.gif
	}
}