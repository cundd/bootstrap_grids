# Page TSConfig:
tx_gridelements.setup {
	# ID of Element
	1200 {
		title = 3 columns
		description = Inserts a 3-columns grid for content.
		config {
			colCount = 3
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.leftColumn
							colPos = 101
						}
						2 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.centerColumn
							colPos = 102
						}
						3 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.rightColumn
							colPos = 103
						}
					}
				}
			}
		}
		icon = EXT:bootstrap_grids/Resources/Public/Icons/gridlayout_col3.gif
		flexformDS =FILE:EXT:bootstrap_grids/Configuration/FlexForm/flexform_3col.xml
	}
}