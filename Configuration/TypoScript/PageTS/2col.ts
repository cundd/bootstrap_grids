# Page TSConfig:
tx_gridelements.setup {
	# ID of Element
	1100 {
		title = 2 columns
		description = Inserts a 2-columns grid for content. The width of each column may be set for different screen sizes.
		config {
			colCount = 2
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.leftColumn
							colPos = 101
						}
						2 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.rightColumn
							colPos = 102
						}
					}
				}
			}
		}
		icon = EXT:bootstrap_grids/Resources/Public/Icons/gridlayout_col2.gif
		flexformDS =FILE:EXT:bootstrap_grids/Configuration/FlexForm/flexform_2col.xml
	}
}