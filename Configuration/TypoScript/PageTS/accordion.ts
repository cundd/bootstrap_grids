# Page TSConfig:
tx_gridelements.setup {
	# ID of Element
	1700 {
		title = Accordion from content
		description = Each content element within the grid represents an accordion element. The title fields are used as accordion bar titles.
		config {
			colCount = 1
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.accordionElements
							colPos = 101
						}
					}
				}
			}
		}
		icon = EXT:bootstrap_grids/Resources/Public/Icons/grid_accordion.png
		flexformDS =FILE:EXT:bootstrap_grids/Configuration/FlexForm/flexform_accordion.xml
	}
}