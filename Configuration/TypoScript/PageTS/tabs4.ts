# Page TSConfig:
tx_gridelements.setup {
	# ID of Element
	1500 {
		title = Tabs (4)
		description = For multiple content elements in one tab. 4 or less tabs.
		config {
			colCount = 4
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.tab1
							colPos = 101
						}
						2 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.tab2
							colPos = 102
						}
						3 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.tab3
							colPos = 103
						}
						4 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.tab4
							colPos = 104
						}
					}
				}
			}
		}
		icon = EXT:bootstrap_grids/Resources/Public/Icons/grid_tabs4.png
		flexformDS =FILE:EXT:bootstrap_grids/Configuration/FlexForm/flexform_tabs4.xml
	}
}