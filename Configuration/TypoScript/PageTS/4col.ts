# Page TSConfig:
tx_gridelements.setup {
	# ID of Element
	1300 {
		title = 4 columns
		description = Inserts a 4-columns grid for content.
		config {
			colCount = 4
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.leftColumn
							colPos = 101
						}
						2 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.column2
							colPos = 102
						}
						3 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.column3
							colPos = 103
						}
						4 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.rightColumn
							colPos = 104
						}
					}
				}
			}
		}
		icon = EXT:bootstrap_grids/Resources/Public/Icons/gridlayout_col4.gif
		flexformDS =FILE:EXT:bootstrap_grids/Configuration/FlexForm/flexform_4col.xml
	}
}