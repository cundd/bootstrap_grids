# Page TSConfig:
tx_gridelements.setup {
	# ID of Element
	1600 {
		title = Tabs (6)
		description = For multiple content elements in one tab. 6 or less tabs.
		config {
			colCount = 6
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name =LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.tab1
							colPos = 101
						}
						2 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.tab2
							colPos = 102
						}
						3 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.tab3
							colPos = 103
						}
						4 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.tab4
							colPos = 104
						}
						5 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.tab5
							colPos = 105
						}
						6 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.tab6
							colPos = 106
						}
					}
				}
			}
		}
		icon = EXT:bootstrap_grids/Resources/Public/Icons/grid_tabs6.png
		flexformDS =FILE:EXT:bootstrap_grids/Configuration/FlexForm/flexform_tabs6.xml
	}
}