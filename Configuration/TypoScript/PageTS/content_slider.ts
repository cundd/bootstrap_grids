# Page TSConfig:
tx_gridelements.setup {
	# ID of Element
	1800 {
		title = Content slider
		description = Each content element within the grid represents a slider element.
		config {
			colCount = 1
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.sliderElements
							colPos = 101
						}
					}
				}
			}
		}
		icon = EXT:bootstrap_grids/Resources/Public/Icons/grid_slider.png
		flexformDS =FILE:EXT:bootstrap_grids/Configuration/FlexForm/flexform_slider.xml
	}
}