# Page TSConfig:
tx_gridelements.setup {
	# ID of Element
	1400 {
		title = Tabs from content
		description = Each content element within the grid represents a tab. The title fields are used as tab titles.
		config {
			colCount = 1
			rowCount = 1
			rows {
				1 {
					columns {
						1 {
							name = LLL:EXT:bootstrap_grids/Resources/Private/Language/locallang_db.xlf:celayout.tabElements
							colPos = 101
						}
					}
				}
			}
		}
		icon = EXT:bootstrap_grids/Resources/Public/Icons/grid_simpletabs.png
		flexformDS =FILE:EXT:bootstrap_grids/Configuration/FlexForm/flexform_tabs_simple.xml
	}
}